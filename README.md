# KI Lehre mit Jupyter Notebooks
## Fortran Kernel auf Taurus
### 1 Venv einrichten
```shell
module load Python/3.8.6-GCCcore-10.2.0
mkdir user-kernel
cd user-kernel
virtualenv --system-site-packages fortran-kernel
source fortran-kernel/bin/activate
pip install ipykernel
pip install --upgrade pip
python -m ipykernel install --user --name fortran-kernel --display-name="Fortran kernel"
```
### 2 fortran-kernel installieren
```shell
git clone https://github.com/sourceryinstitute/jupyter-CAF-kernel
cd jupyter-CAF-kernel
pip install -e prebuild/jupyter-caf-kernel
jupyter-kernelspec install --user prebuild/jupyter-caf-kernel/Coarray-Fortran/
```
### 3 Konfigurieren
Verzeichnis wechseln

`cd ~/.local/share/jupyter/kernels/fortran-kernel`

*kernel.json* editieren

`nano kernel.json`

Folgenden Inhalt einfügen:
```shell
{
 "argv": [
  "/home/<USER>/user-kernel/fortran-kernel/bin/loader.sh",
  "-m",
  "jupyter_caf_kernel",
  "-f",
  "{connection_file}"
 ],
 "display_name": "Fortran kernel",
 "language": "Fortran"
}
```
Datei speichern und schließen. Als nächstes *loader.sh* erstellen:

`nano ~/user-kernel/fortran-kernel/bin/loader.sh`

Folgenden Inhalt einfügen:
```shell
#!/usr/bin/bash

module load Python
/home/<USER>/user-kernel/fortran-kernel/bin/python "$@"
```
Speichern und schließen. Ausführbar machen:

`chmod +x ~/user-kernel/fortran-kernel/bin/loader.sh`

Jetzt zurück in die venv

`cd ~/user-kernel/fortran-kernel`

und deaktivieren

`deactivate`

Fertig :)
## Klonen
Link Generator:

`https://jupyterhub.github.io/nbgitpuller/link?hub=https://taurus.hrsk.tu-dresden.de/jupyter/`

Für das Lab:

`https://taurus.hrsk.tu-dresden.de/jupyter/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.hrz.tu-chemnitz.de%2Fs8966401--tu-dresden.de%2Fki-lehre.git&urlpath=lab%2Ftree%2Fki-lehre.git%2FEinfuehrung.ipynb&branch=main`

Für ein Notebook:

`https://taurus.hrsk.tu-dresden.de/jupyter/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.hrz.tu-chemnitz.de%2Fs8966401--tu-dresden.de%2Fki-lehre.git&urlpath=tree%2Fki-lehre.git%2FEinfuehrung.ipynb&branch=main`
